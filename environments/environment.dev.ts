export const environment = {
  production: false,
  gql: {
    url: 'http://localhost:4000/graphql'
  }
};
