export const environment = {
  production: true,
  gql: {
    url: 'http://127.0.0.1:4000/graphql'
  }
};
