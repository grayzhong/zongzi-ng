
export abstract class PopupService {
  abstract alert(message: string, titleIn: 'Success' | 'Error', callback?: () => void);
  abstract toast(message: string, callback?: () => void);
}
