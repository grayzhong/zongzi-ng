import { User, Admin, UserLoginResult, AdminLoginResult, RoleVer } from '../gql.types';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


export abstract class AuthService {
  abstract lastUrl: string;
  abstract token: string;
  abstract roleVer: RoleVer;
  abstract currentUser: User;
  abstract currentAdmin: Admin;
  abstract hasLogin: boolean;

  abstract init(navCtrl: any);

  abstract loginSuccess(loginResult: UserLoginResult | AdminLoginResult);

  abstract authFail({ networkError, graphQLErrors }: any);

  abstract canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot);

  abstract setApollo(apollo: any);

  abstract logout();
}

