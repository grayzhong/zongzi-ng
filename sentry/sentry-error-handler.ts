import * as Raven from 'raven-js';
import { ErrorHandler } from '@angular/core';
import { environment } from '../environments/environment.dev';
// @ts-ignore
import * as packageJson from "../../../package.json";

let sentryUrl = '';
if (packageJson.name === 'tuiwo-app') {
  sentryUrl = 'https://b405e3d18bb344c29c8a093a85bd55ec@sentry.io/1262971';
} else if (packageJson.name === 'admin-web') {
  sentryUrl = 'https://70739fee88094fc1a3dfaf2201fa0c5f@sentry.io/1263325';
}

Raven
  .config(sentryUrl, { release: packageJson.version })
  .install();

export class SentryErrorHandler extends ErrorHandler {
  handleError(err: any): void {
    Raven.captureException(err.originalError || err);
    if (!environment.production) {
      super.handleError(err);
    }
  }
}
