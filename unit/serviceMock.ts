export class PopupServiceMock {
  alert = jest.fn().mockReturnValue(Promise.resolve());
  toast = jest.fn();
}

export class AuthServiceMock {
  loginSuccess = jest.fn();
}

export class NavControllerMock {
  setRoot = jest.fn();
  goToRoot = jest.fn();
  push = jest.fn();
  registerChildNav = jest.fn();
}

export class NavParamsMock {
  get(key) {
    return 9
  }
}

export class LoadingControllerMock {
  create = jest.fn().mockReturnValue({
    present: jest.fn(),
    dismiss: jest.fn(),
  });
}

export class AppMock {
  getRootNav() {
    return {
      last: '',
      setRoot: jest.fn()
    }
  }
}

export class AlertControllerMock {
  create = jest.fn()
    .mockImplementation(({ buttons }) => {
      const btn = buttons[0];
      if (btn && btn.handler) {
        btn.handler();
      }
      return {
        present: jest.fn(),
        dismiss: jest.fn(),
      }
    })
}

export class ToastControllerMock {
  create = jest.fn()
    .mockImplementation(({ buttons }) => {
      const btn = buttons[0];
      if (btn && btn.handle) {
        btn.handle();
      }
    })
    .mockReturnValue({
      present: jest.fn(),
      dismiss: jest.fn(),
    });
}
