import { buildClientSchema } from 'graphql';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SchemaLink } from 'apollo-link-schema';
import { addMockFunctionsToSchema } from 'graphql-tools';
import { Apollo } from 'apollo-angular';
// @ts-ignore
import * as schemaJSON from '../../../../graphql.schema.json';

const schema = buildClientSchema(schemaJSON.data);

type Resolves = {
  [key: string]: object | (() => object)
}
function apolloCreate(apollo: Apollo, queryResolvers: Resolves, mutationResolvers: Resolves) {
  addMockFunctionsToSchema({
    schema,
    mocks: {
      Query() {
        return queryResolvers;
      },
      Mutation() {
        return mutationResolvers;
      },
      BigInt: () => {
        return Date.now()
      }
    }
  });
  apollo.create({
    cache: new InMemoryCache((window as any).__APOLLO_STATE__),
    link: new SchemaLink({ schema })
  });
  return apollo;
}

export function apolloCreateQuery(apollo: Apollo, queryResolvers: Resolves) {
  return apolloCreate(apollo, queryResolvers, {});
}
export function apolloCreateMuation(apollo, mutationResolvers: Resolves) {
  return apolloCreate(apollo, {}, mutationResolvers)
}
