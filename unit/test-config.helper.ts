import { TestBed } from '@angular/core/testing'
import { Type } from '@angular/core';
import { Apollo, ApolloModule } from 'apollo-angular';
import { IonicModule, LoadingController, NavController, NavParams } from 'ionic-angular';
import {
  AuthServiceMock,
  LoadingControllerMock,
  NavControllerMock,
  NavParamsMock,
  PopupServiceMock
} from './serviceMock';
import { TestModuleMetadata } from '@angular/core/testing/src/test_bed';
import { PopupService } from '../abstract/PopupService';
import { AuthService } from '../abstract/AuthService';

type CompilerOptions = Partial<{
  schemas: any[];
  providers: any[];
  useJit: boolean;
  preserveWhitespaces: boolean;
}>
export type ConfigureFn = (testBed: typeof TestBed) => void

export function configureTests(configure: ConfigureFn, compilerOptions: CompilerOptions = {}) {
  const compilerConfig: CompilerOptions = {
    // schemas: [CUSTOM_ELEMENTS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    preserveWhitespaces: false,
    ...compilerOptions,
  }

  const configuredTestBed = TestBed.configureCompiler(compilerConfig)

  configure(configuredTestBed)

  return configuredTestBed.compileComponents().then(() => configuredTestBed)
}
export function configureApolloTests<PageClass>(
  MyPage: Type<PageClass>,
  opt: TestModuleMetadata = {}) {
  return configureTests(testBed => {
    testBed
      .configureTestingModule({
        declarations: [MyPage, ...(opt.declarations||[])],
        providers: [
          NavControllerMock,
          NavParamsMock,
          PopupServiceMock,
          AuthServiceMock,
          LoadingController,
          { provide: NavController, useClass: NavControllerMock },
          { provide: NavParams, useClass: NavParamsMock },
          { provide: LoadingController, useClass: LoadingControllerMock },
          { provide: PopupService, useClass: PopupServiceMock },
          { provide: AuthService, useClass: AuthServiceMock },
          ...(opt.providers || []),
        ],
        imports: [
          ApolloModule,
          IonicModule.forRoot(MyPage),
        ],
      });
  })
    .then(testBed => {
      const fixture = testBed.createComponent(MyPage);
      const component = fixture.componentInstance;
      fixture.detectChanges();
      const apollo = testBed.get(Apollo);
      const navCtrl = testBed.get(NavController);
      const popupService = testBed.get(PopupService);
      const authService = testBed.get(AuthService);
      const loadingCtrl = testBed.get(LoadingController);
      return {
        fixture,
        component,
        apollo,
        navCtrl,
        popupService,
        authService,
        testBed,
        loadingCtrl,
      }
    })
}
