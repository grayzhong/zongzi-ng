import { ApolloLink, Observable, } from 'apollo-link';
jest.doMock('apollo-link-context', () => ({
  setContext: (setter) =>
    new ApolloLink((operation: any, forward: any) => {
      setter(operation);
      return new Observable(observer => {
        const handle = forward(operation).subscribe({
          next: observer.next.bind(observer),
          error: observer.error.bind(observer),
          complete: observer.complete.bind(observer),
        });
        return () => {
          if (handle) handle.unsubscribe();
        };
      });
    })
}));
