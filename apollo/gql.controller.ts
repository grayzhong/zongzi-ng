import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { setContext } from 'apollo-link-context';
import { HttpHeaders } from '@angular/common/http';
import { onError } from 'apollo-link-error';
import { AuthService } from '../abstract/AuthService';
import { WebSocketLink } from 'apollo-link-ws';
import { split } from 'apollo-link';
import { getMainDefinition } from 'apollo-utilities';
import { OperationDefinitionNode } from 'graphql';
// @ts-ignore
import * as packageJson from "../../../package.json";
import { camelCase } from 'lodash';
import { QueryManager } from '../../../node_modules/apollo-client/core/QueryManager';
import { FetchType, WatchQueryOptions } from 'apollo-client';
import * as Raven from 'raven-js';
import { QueryRef } from 'apollo-angular/QueryRef';
import { R } from 'apollo-angular/types';
import { TypedVariables } from '../../../node_modules/apollo-angular/types';

export interface PageResult<T> {
  total: number;
  items: T[]
  offset?: number;
  limit?: number;
}
@Injectable()
export class GqlController {
  queryMap: Map<string, QueryRef<any>> = new Map<string, QueryRef<any>>();

  constructor(public apollo: Apollo,
              public httpLink: HttpLink,
              public authService: AuthService) {
  }

  createApolloClient(
    {
      url = 'http://localhost:4000/graphql',
    }
  ) {
    const cache = new InMemoryCache();
    const http = this.httpLink.create({ uri: url });
    const headerFn = () => {
      let headers = this.authService.token
        ? new HttpHeaders().set('Authorization', `Bearer ${this.authService.token}`)
        : new HttpHeaders();
      headers = headers.set('X-Product', camelCase(packageJson.name) + '@' + packageJson.version);
      headers = headers.set('X-RoleVer', window.localStorage.getItem('roleVer'));
      return { headers };
    };
    const tokenLink = setContext(headerFn);
    const wsClient = new WebSocketLink({
      uri: `ws://localhost:4000/subscriptions`,
      options: {
        reconnect: true,
        connectionParams: {
          'X-Product': camelCase(packageJson.name) + '@' + packageJson.version,
          'X-RoleVer': window.localStorage.getItem('roleVer'),
        }
      },
    });
    Raven.setTagsContext({
      'X-Product': camelCase(packageJson.name) + '@' + packageJson.version,
      'X-RoleVer': window.localStorage.getItem('roleVer'),
    });
    const linkOk = split(
      // split based on operation type
      ({ query }) => {
        const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode;
        return kind === 'OperationDefinition' && operation === 'subscription';
      },
      wsClient,
      http,
    );
    const link = tokenLink.concat(onError(this.authService.authFail).concat(linkOk));
    this.apollo.create({
      link,
      cache,
    });
    const state = cache.extract();
    console.info('☞☞☞ cache', state);
    // console.info('☞☞☞ cache', this.apollo.getClient());
    this.authService.setApollo(this.apollo);
    return this.apollo;
  }

  watchQuery<T, V = R>(options: WatchQueryOptions & TypedVariables<V>): QueryRef<T> {
    const queryRef = this.apollo.watchQuery<T, V>(options);
    const [definition] = options.query.definitions as OperationDefinitionNode[];
    const operationName = definition.name.value;
    this.queryMap.set(operationName, queryRef);
    return queryRef;
  }

  pop(operationName: string, id: number) {
    const queryManager = this.apollo.getClient().queryManager as any;
    const queryIds = queryManager.queryIdsByName[operationName] || [NaN];
    const query = queryManager.getQuery(queryIds[0]);
    const result = this.apollo.getClient().readQuery({ query: query.document });
    const resultOk = result[operationName].filter(v => v.id !== id);
    this.apollo.getClient().writeQuery({
      query: query.document,
      data: { [operationName]: resultOk }
    })
  }

  push(operationName: string, item: any, isHead: boolean = true) {
    const queryManager = this.apollo.getClient().queryManager as any;
    const [queryId] = queryManager.queryIdsByName[operationName] || [NaN];
    if (queryId) {
      const query = queryManager.getQuery(queryId);
      const allResult = this.apollo.getClient().readQuery({ query: query.document });
      const result = allResult[operationName] || [];
      const resultOk = isHead ? [item, ...result] : [...result, item];
      this.apollo.getClient().writeQuery({
        query: query.document,
        data: { [operationName]: resultOk }
      });
      return true;
    } else {
      return false;
    }
  }

  removeWatch(operationName: string) {
    const queryManager = this.apollo.getClient().queryManager as any;
    const [queryId] = queryManager.queryIdsByName[operationName] || [NaN];
    queryManager.removeObservableQuery(queryId);
  }

  refetch(operationName: string, variables: any) {
    const queryManager = this.apollo.getClient().queryManager as any;
    const queryIds = queryManager.queryIdsByName[operationName] || [NaN];
    const query = queryManager.getQuery(queryIds[0]);
    (queryManager as QueryManager<any>).fetchQuery(queryIds[0],
      { query: query.document, variables, fetchPolicy: 'cache-and-network' }, FetchType.normal)
  }

  get(operationName: string, defaultValue?: any[]) {
    const queryManager = this.apollo.getClient().queryManager as any;
    const queryIds = queryManager.queryIdsByName[operationName] || [NaN];
    const query = queryManager.getQuery(queryIds[0]);
    const result = this.apollo.getClient().readQuery({ query: query.document });
    return result || defaultValue;
  }

  save(operationName: string, values: any[]) {
    const queryManager = this.apollo.getClient().queryManager as any;
    const queryIds = queryManager.queryIdsByName[operationName] || [NaN];
    const query = queryManager.getQuery(queryIds[0]);
    this.apollo.getClient().writeQuery({
      query: query.document,
      data: { [operationName]: values }
    })
  }
}
