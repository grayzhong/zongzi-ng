import { NgModule } from '@angular/core';
import { HttpLinkModule } from 'apollo-angular-link-http';
import { ApolloModule } from 'apollo-angular';
import { GqlController } from './gql.controller';

@NgModule({
  imports: [
    HttpLinkModule,
    ApolloModule
  ],
  providers: [
    GqlController,
  ]
})
export class GqlModule {}
