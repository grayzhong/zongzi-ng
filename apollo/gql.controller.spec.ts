import { async } from '@angular/core/testing';
import { GqlController } from './gql.controller';
import { Apollo } from 'apollo-angular';
import { HttpLinkModule } from 'apollo-angular-link-http';
import gql from 'graphql-tag';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthService }from '../abstract/AuthService';
import { AuthServiceMock } from '../unit/serviceMock';
import { ConfigureFn, configureTests } from '../unit/test-config.helper';


describe('gql controller', () => {

  let httpTestingController: HttpTestingController;
  let gqlCtrl: GqlController;

  beforeEach(
    async(() => {
      const configure: ConfigureFn = testBed => {
        testBed
          .configureTestingModule({
            declarations: [],
            imports: [HttpLinkModule, HttpClientTestingModule],
            providers: [
              GqlController,
              Apollo,
              { provide: AuthService, useClass: AuthServiceMock },
            ],
          })
        ;
        httpTestingController = testBed.get(HttpTestingController);
        gqlCtrl = testBed.get(GqlController);
      };

      configureTests(configure);
    })
  );

  it(`request gql`, ((done) => {
    expect(gqlCtrl).toBeTruthy();
    httpTestingController.verify();
    gqlCtrl.apollo.query({
      query: gql`{ users {id} }`
    })
    .subscribe((v: any) => {
      expect(v).toBeFalsy();
    }, (e) => {
      expect(e.message).toMatch('Unauthorized');
      done();
    }, () => {
      done();
    });
    const req = httpTestingController.expectOne('http://localhost:4000/graphql');
    req.flush('', { status: 401, statusText: 'Unauthorized' });
    httpTestingController.verify();

    const state = gqlCtrl.apollo.getClient().cache.extract();
    expect(state).toBeDefined();
    expect(JSON.stringify(state)).toBe('{}');
  }));

});
