/* tslint:disable */

/** Date custom scalar type */
export type Date = any;

/** The `BigInt` scalar type represents non-fractional signed whole numeric values. BigInt can represent values between -(2^53) + 1 and 2^53 - 1. */
export type BigInt = any;

/** The `Upload` scalar type represents a file upload promise that resolves an object containing `stream`, `filename`, `mimetype` and `encoding`. */
export type Upload = any;

export interface Query {
  users: UserPageResult;
  admins: AdminPageResult;
  admin?: Admin | null;
  dashboard: Dashboard;
  issues: IssuePageResult;
  issue?: Issue | null;
  permissions: PermissionPageResult;
  permission?: Permission | null;
  permissionReload?: boolean | null;
  publishes: PublishPageResult;
  publishesLatest: Publish[];
  publish: Publish;
  publishLog?: boolean | null;
  publishDetection: Publish;
  resumes: ResumePageResult;
  resume?: Resume | null;
  settings: Setting[];
  setting: Setting;
  tables?: Table[] | null;
  workmates: WorkmatePageResult;
  workmate: Workmate;
}

export interface UserPageResult {
  offset: number;
  limit: number;
  total: number;
  items: User[];
}

export interface User {
  id: number;
  email: string;
  mobile: string;
  password: string;
  trueName?: string | null;
  shortName?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  gender?: Gender | null;
  createdDate?: Date | null;
  updatedDate?: Date | null;
  resumes: Resume[];
  workmate: Workmate[];
  roleVer: RoleVer;
  roleAuth: RoleAuth;
}

export interface Resume {
  id?: number | null;
  note?: string | null;
  createdDate?: Date | null;
  updatedDate?: Date | null;
  user?: User | null;
}

export interface Workmate {
  id: number;
  email: string;
  mobile: string;
  trueName?: string | null;
  shortName?: string | null;
  firstName?: string | null;
  lastName?: string | null;
  gender?: Gender | null;
  createdDate?: Date | null;
  updatedDate?: Date | null;
}

export interface AdminPageResult {
  offset: number;
  limit: number;
  total: number;
  items: Admin[];
}

export interface Admin {
  id: number;
  email: string;
  mobile: string;
  password: string;
  trueName?: string | null;
  gender?: Gender | null;
  createdDate?: Date | null;
  updatedDate?: Date | null;
  roleVer: RoleVer;
  roleAuth: RoleAuth;
}

export interface Dashboard {
  user?: DashboardItem | null;
  workmate?: DashboardItem | null;
  resume?: DashboardItem | null;
  admin?: DashboardItem | null;
  issue?: DashboardItem | null;
}

export interface DashboardItem {
  total: number;
}

export interface IssuePageResult {
  offset: number;
  limit: number;
  total: number;
  items: Issue[];
}

export interface Issue {
  id: number;
  title: string;
  product: Product;
  sentryId: number;
  shortId: string;
  status: string;
  file: string;
  type: string;
  value: string;
  userCount: number;
  count: number;
  lastSeen: Date;
  firstSeen: Date;
}

export interface PermissionPageResult {
  offset: number;
  limit: number;
  total: number;
  items: Permission[];
}

export interface Permission {
  id: number;
  ptype: string;
  roleVer?: string | null;
  roleAuth: string;
  product?: string | null;
  module?: string | null;
  path?: string | null;
}

export interface PublishPageResult {
  offset: number;
  limit: number;
  total: number;
  items: Publish[];
}

export interface Publish {
  id: number;
  version: string;
  major: number;
  minor: number;
  patch: number;
  content: string;
  product: Product;
  email: string;
  admin?: Admin | null;
  buildStatus?: Status | null;
  buildCommand: string;
  tag: RoleVer /** force: [String!]!notice: [String!]!preload: [String!]!silent: [String!]! */;
  publishDate?: Date | null;
  createdDate?: Date | null;
  updatedDate?: Date | null;
}

export interface ResumePageResult {
  offset: number;
  limit: number;
  total: number;
  items: Resume[];
}

export interface Setting {
  id: number;
  product: Product;
  module: Module;
  name: string;
  value: string;
  note?: string | null;
  updatedDate?: Date | null;
  admin?: Admin | null;
}

export interface Table {
  id: number;
  name: string;
  version: string;
  fields: string;
}

export interface WorkmatePageResult {
  offset: number;
  limit: number;
  total: number;
  items: Workmate[];
}

export interface Mutation {
  userCreate?: User | null;
  userLogin?: UserLoginResult | null;
  userUpdate?: User | null;
  userDelete?: boolean | null;
  adminLogin?: AdminLoginResult | null;
  adminCreate?: Admin | null;
  adminUpdate?: Admin | null;
  adminPwd?: Admin | null;
  adminDelete?: boolean | null;
  issueUpdate?: Issue | null;
  issueDelete?: boolean | null;
  permissionCreate?: Permission | null;
  permissionUpdate?: Permission | null;
  permissionDelete?: boolean | null;
  publishCreate?: Publish | null;
  publishUpdate?: Publish | null;
  publishDelete?: boolean | null;
  resumeCreate?: Resume | null;
  resumeUpdate?: Resume | null;
  resumeDelete?: boolean | null;
  settingCreate?: Setting | null;
  settingUpdate?: Setting | null;
  settingDelete?: boolean | null;
  updateTableVersion?: Table[] | null;
  workmateCreate?: Workmate | null;
  workmateUpdate?: Workmate | null;
  workmateDelete?: boolean | null;
}

export interface UserLoginResult {
  user: User;
  loginInfo: LoginInfo;
  authInfo: AuthInfo;
}

export interface LoginInfo {
  token: string;
  expiration: BigInt;
}

export interface AuthInfo {
  moduleNames: string[];
}

export interface AdminLoginResult {
  admin: Admin;
  loginInfo: LoginInfo;
  authInfo: AuthInfo;
}

export interface Subscription {
  publishBuild?: PublishBuildMessage | null;
  publishLatest: Publish;
}

export interface PublishBuildMessage {
  id: number;
  message: string;
  code?: number | null;
}
export interface UsersQueryArgs {
  offset?: number | null;
  limit?: number | null;
}
export interface AdminsQueryArgs {
  offset?: number | null;
  limit?: number | null;
}
export interface AdminQueryArgs {
  id: number;
}
export interface IssuesQueryArgs {
  product: Product;
  offset?: number | null;
  limit?: number | null;
}
export interface IssueQueryArgs {
  id: number;
}
export interface PermissionsQueryArgs {
  offset?: number | null;
  limit?: number | null;
}
export interface PermissionQueryArgs {
  id: number;
}
export interface PublishesQueryArgs {
  offset?: number | null;
  limit?: number | null;
}
export interface PublishQueryArgs {
  id?: number | null;
}
export interface PublishLogQueryArgs {
  id: number;
  createdDate: Date;
}
export interface PublishDetectionQueryArgs {
  product: Product;
  version: string;
}
export interface ResumesQueryArgs {
  offset?: number | null;
  limit?: number | null;
}
export interface ResumeQueryArgs {
  id?: number | null;
}
export interface SettingQueryArgs {
  id: number;
}
export interface WorkmatesQueryArgs {
  offset?: number | null;
  limit?: number | null;
}
export interface WorkmateQueryArgs {
  id: number;
}
export interface UserCreateMutationArgs {
  mobile?: string | null;
  email?: string | null;
  password?: string | null;
}
export interface UserLoginMutationArgs {
  email?: string | null;
  password?: string | null;
}
export interface UserUpdateMutationArgs {
  id?: number | null;
  email?: string | null;
  firstName?: string | null;
  lastName?: string | null;
}
export interface UserDeleteMutationArgs {
  id?: number | null;
}
export interface AdminLoginMutationArgs {
  email?: string | null;
  password?: string | null;
}
export interface AdminCreateMutationArgs {
  email: string;
  mobile: string;
  password: string;
  trueName?: string | null;
  roleVer?: RoleVer | null;
  roleAuth?: RoleAuth | null;
}
export interface AdminUpdateMutationArgs {
  id?: number | null;
  email: string;
  mobile: string;
  trueName?: string | null;
  roleVer?: RoleVer | null;
  roleAuth?: RoleAuth | null;
}
export interface AdminPwdMutationArgs {
  password: string;
  passwordNew: string;
}
export interface AdminDeleteMutationArgs {
  id?: number | null;
}
export interface IssueUpdateMutationArgs {
  id: number;
  product: Product;
  status: string;
}
export interface IssueDeleteMutationArgs {
  id: number;
  product: Product;
}
export interface PermissionCreateMutationArgs {
  roleVer: string;
  roleAuth: string;
  product: string;
  module: string;
  path: string;
}
export interface PermissionUpdateMutationArgs {
  id?: number | null;
  roleVer: string;
  roleAuth: string;
  product: string;
  module: string;
  path: string;
}
export interface PermissionDeleteMutationArgs {
  id?: number | null;
}
export interface PublishCreateMutationArgs {
  version: string;
  major: number;
  minor: number;
  patch: number;
  content: string;
  product: Product;
  buildCommand: string;
}
export interface PublishUpdateMutationArgs {
  id: number;
  version: string;
  content: string;
  product: Product;
  tag: RoleVer;
}
export interface PublishDeleteMutationArgs {
  id: number;
}
export interface ResumeCreateMutationArgs {
  note?: string | null;
}
export interface ResumeUpdateMutationArgs {
  id?: number | null;
  note?: string | null;
}
export interface ResumeDeleteMutationArgs {
  id?: number | null;
}
export interface SettingCreateMutationArgs {
  value: string;
  product: Product;
  module: Module;
  name: string;
  note?: string | null;
}
export interface SettingUpdateMutationArgs {
  id: number;
  value: string;
  note?: string | null;
}
export interface SettingDeleteMutationArgs {
  id: number;
}
export interface WorkmateCreateMutationArgs {
  trueName?: string | null;
  shortName?: string | null;
  mobile?: string | null;
  email?: string | null;
  gender?: Gender | null;
}
export interface WorkmateUpdateMutationArgs {
  id?: number | null;
  email?: string | null;
  mobile?: string | null;
  trueName?: string | null;
  shortName?: string | null;
  gender?: Gender | null;
}
export interface WorkmateDeleteMutationArgs {
  id?: number | null;
}
export interface PublishLatestSubscriptionArgs {
  product: Product;
  version: string;
}

export enum Gender {
  man = "man",
  girl = "girl",
  secret = "secret"
}

export enum RoleVer {
  free = "free",
  latest = "latest",
  beta = "beta",
  alpha = "alpha",
  dev = "dev"
}

export enum RoleAuth {
  none = "none",
  guest = "guest",
  member = "member",
  admin = "admin",
  super = "super"
}

export enum Product {
  all = "all",
  server = "server",
  adminWeb = "adminWeb",
  tuiwoApp = "tuiwoApp"
}

export enum Status {
  pending = "pending",
  success = "success",
  fail = "fail"
}

export enum Module {
  none = "none",
  admin = "admin",
  dashboard = "dashboard",
  publish = "publish",
  resume = "resume",
  table = "table",
  user = "user",
  workmate = "workmate"
}
